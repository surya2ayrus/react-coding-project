import {LOGIN, LOGOUT, CHANGE_USER} from '../Action/Action'

const reducer = function(state,action){
    console.log("reducer",LOGIN,LOGOUT);
    switch(action.type){
        case LOGIN:
            console.log("inside login");
            return {...state, isLoggedIn: true};
        case LOGOUT:
            return {...state, isLoggedIn: false};
        case CHANGE_USER:
            console.log("change user")
            return {...state, role: action.role};
        default:
            return state
    }
}

export default reducer;