import React from "react";
import { hot } from 'react-hot-loader/root';
import Login from './Component/LoginComponent/Login'
import '../css/login.css'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Dashboard from "./Component/DashboardComponent/DashboardComponent";
import ShipsComponent from './Component/DashboardComponent/ShipsComponent'
import SingleShipComponent from './Component/DashboardComponent/SingleShipComponent'

class App extends React.Component {    
   render() {   
      return (
            <Router history= {history} >
               <Switch>
                  <Route path="/" exact component={Login} history={history} />
                  <Route path="/dashboard" component={Dashboard} />
                  <Route path="/ships/:id" component={SingleShipComponent} />
                  <Route path="/ships" exact component={ShipsComponent} />
                  
               </Switch>
           </Router>
         
      );
   }
}
export default hot(App);