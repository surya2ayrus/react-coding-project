import React, { Component } from 'react'
import NavbarComponent  from './NavbarComponent';
// import {connect} from 'react-redux';

class SingleShipComponent extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    fetchShip = async() => {
        // console.log("inside async function props:",this.props)
        let ship_id = this.props.match.params.id
        
        let shipData = await fetch(`https://api.spacexdata.com/v3/ships/${ship_id}`);
        shipData = await shipData.json();
        // console.log("ship data",shipData)
        this.setState({
            ship_data: shipData
        },() => {
            // console.log("data inserted",this.state)
        })
    }

    componentDidMount(){
        // let ls = localStorage.getItem('state');
        // console.log("local storage",ls);
        this.fetchShip();
        
    }

    render() {
        console.log("props in single ship component",this.props);
        let ship = this.state.ship_data || '';
        if(ship){
            // console.log("image",ship)
            ship = <div className="card bg-dark text-black">
                        <img src={ship.image} className="card-img" alt="Image Not Found" />
                            <div className="card-img-overlay">
                                <h5 className="card-title"> {ship.ship_name} </h5>
                                <p className="card-text"> {
                                    ship.missions.map((mission,index) => {
                                        return (<div key={index}>
                                            <div style={{fontSize: '12px'}}><em>Mission Name: {mission.name} </em></div>
                                            <div style={{fontSize: '12px'}}><em> Mission Flight: {mission.flight} </em></div>
                                        </div>)
                                    })
                                } </p>
                                
                        </div>
                    </div>
        } else {
            ship = <h1> Loading... </h1>
        }
        return (
            <div>
                <NavbarComponent />
                {ship}
            </div>
        )
    }
}

// const mapStateToProps = (state)=>{
//     return state;
// }


export default SingleShipComponent
