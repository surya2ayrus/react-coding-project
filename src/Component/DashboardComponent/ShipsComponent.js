import React, { Component } from 'react'
import NavbarComponent from './NavbarComponent'
import '../../../css/dashboard.css'
import { Link } from 'react-router-dom'
// import { connect } from 'react-redux'


export class ShipsComponent extends Component {
    
    constructor(props) {
        super(props)
        this.state = {
             
        }
    }
    

    shipsData = async() => {
        
        let data = await fetch("https://api.spacexdata.com/v3/ships");
        data = await data.json()
        this.setState({
            ships: data
        },()=>{
            // console.log("ships data",this.state.ships);
        })
    }

    componentDidMount(){
        console.log("component mounted");
       this.shipsData();
    }

    test = (ship_id) => {
        this.props.history.push(`/ships/${ship_id}`);
    }

    render() {
        console.log("props in ships component",this.props)
        let shipData;
        if(this.state.ships){
            shipData = this.state.ships.map((ship,index) => {
                return (
            
                <div className="jumbotron col-4 bg-black" key={ship.ship_id} > 
                    {/* <Link to={`/ships/${ship.ship_id}`}> */}
                        <div className="d-flex justify-content-around" onClick ={() => this.test(ship.ship_id).bind(this)} >
                            <div className="card" style={{width: '18rem'}}>
                                <img src={`${ship.image}`} className="card-img-top" height="250px" width="250px" alt="Image Not Found" />
                                <div className="card-body">
                                    <p className="card-text lead">Name: {ship.ship_name} </p>
                                    <p className="card-text">Type: {ship.ship_type} </p>
                                    <p className="card-text">Roles: {ship.roles.toString()} </p>
                                </div>
                            </div>
                        </div>
                    {/* </Link> */}
                </div>
           
                )
            })
        }

        return (
            <div>
                <NavbarComponent />
                <div className="row">
                    { shipData }
                </div>
            </div>
        )
    }
}

export default ShipsComponent
