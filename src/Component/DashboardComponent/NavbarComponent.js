import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../../../css/dashboard.css'
import {connect} from 'react-redux';
import { CHANGE_USER, LOGOUT } from '../../Action/Action';

class NavbarComponent extends Component {
    
    logoutHandler = () => {
        console.log("props when logout handler called",this.props);
        this.props.dispatch({type:CHANGE_USER,role: ''});
        this.props.dispatch({type:LOGOUT});
    }
    
    render() {
        console.log("props in nav component",this)
        return (
            <div>
                <nav className="navstyle">
                <div className="dashboard-text">
                    Welcome Aboard, <b><em> {this.props.role}</em></b>
                </div>
                    <div>
                        <Link className="dashboard-navstyle" to="/dashboard">                            
                                <h3>Home</h3>
                        </Link>
                    </div>
                    <div>
                        <Link className="dashboard-navstyle" to="/ships">                            
                                <h3>Ships</h3>
                        </Link>
                    </div>
                    <div>
                        <Link className="dashboard-navstyle" to="/" onClick={ this.logoutHandler}>                            
                                <h3>Logout</h3>
                        </Link>
                    </div>
                </nav>
            </div>
        )
    }
}

const mapStateToProps = (state)=>{
    return state
}

// const mapDispatchToProps = ()

export default connect(mapStateToProps)(NavbarComponent)
