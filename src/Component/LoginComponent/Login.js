import React, { Component } from 'react'
import Input from './Input';
import SubmitButton from './SubmitButton';
import {connect} from 'react-redux';
import {CHANGE_USER, LOGIN,LOGOUT} from '../../Action/Action'


export class Login extends Component {

    constructor(props) {
        super(props)
    
        this.state = { 
        }
    }

    fetchCredentials = async() => { 
        let credentials = await fetch("https://run.mocky.io/v3/90d43b36-edd5-47f6-8f02-7498d537f7e1");
        credentials = await credentials.json();
        // {
        //     "username": "user",
        //     "password": "user",
        //     "role": "admin"
        // }
        console.log("creds",credentials);
        if(this.state.Username != credentials.username && this.state.Password != credentials.password) {
            this.setState({
                error: "credentials_dont_match"
            })
            return false;
        }
        console.log("role",credentials.role);
        this.props.dispatch({type:CHANGE_USER, role: credentials.role});
        this.props.dispatch({type:LOGIN});
        // console.log("props",this.props)
        
        return true;
    }

    onChangeHandler(e){
        let attr = e.target.ariaLabel
        this.setState({
            [attr] : e.target.value,
            error : false
        },()=>{
            // console.log("state",this.state)
        })
    }

    onSubmit(){
        // console.log("invoking on submit",JSON.stringify(this.state) == "{}")
        if(JSON.stringify(this.state) == "{}"){
            // console.log("set state")
            this.setState({
                error:"empty"
            })
        }
        if(this.state.Username == (null|| "") || this.state.Password == (null||"")){
            this.setState({
                error: "insufficient_fields"
            })
        }
        this.fetchCredentials().then((res) => {
            if(res){
                // console.log(this.props);
                this.props.history.push("/dashboard");
            }
        })
    }

    render() {
        let errorRes;
        // console.log("error ",errorRes)
        if(this.state.error == "empty"){
            errorRes = <span className="danger"> please fill username and password </span>
        } else if(this.state.error == "insufficient_fields"){
            errorRes = <span className="danger"> please enter all the fields </span>
        } else if(this.state.error == "credentials_dont_match"){
            errorRes = <span className="danger">  Invalid login or password </span>
        }
        else {
            errorRes = "";
        }
        return (
            <div className="d-flex align-items-center flex-wrap justify-content-center login-component">
                <div className="jumbotron col-6 ">
                    <div className="display-4 header">Login</div>
                    
                    <Input inputName={"Username"} inputSymbol={"@"} onChangeHandler={this.onChangeHandler.bind(this)} />
                    {errorRes}
                    <Input inputName={"Password"} inputSymbol={"***"} onChangeHandler={this.onChangeHandler.bind(this)} />   
                    {errorRes}
                    <SubmitButton onSubmit = {this.onSubmit.bind(this)} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state)=>{
    return state
}

export default connect(mapStateToProps)(Login);
