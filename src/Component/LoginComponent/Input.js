import React, { Component } from 'react'

export class Input extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            field: ""
        }
    }

    render() {
        // console.log(this.props)
        return (
            <div className="input-group flex-nowrap container extra-space">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="addon-wrapping"> {this.props.inputSymbol} </span>
                </div>
                <input 
                type="text" 
                className="form-control" 
                placeholder={`${this.props.inputName}`} 
                aria-label={`${this.props.inputName}`} 
                aria-describedby="addon-wrapping" 
                onChange={(e) => this.props.onChangeHandler(e)} required/>
            </div>
        )
    }
}

export default Input
