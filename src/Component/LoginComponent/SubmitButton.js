import React, { Component } from 'react'
import '../../../css/login.css'


export class SubmitButton extends Component {
    
    render() {
            
        return (
            <div className="button-placement">
                <input 
                className="btn btn-primary" 
                type="submit" 
                value="Submit" 
                onClick={(e) => this.props.onSubmit(e)}
                />
            </div>
        )
    }
}

export default SubmitButton
